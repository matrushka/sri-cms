(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.partnerCard = {
    attach: function (context, settings) {
      var $partnerCards = $(".partner-card", context);

      if($partnerCards.length > 0) {

        function toggleFlipped(element) {
          var isFlipped = element.hasClass('is-flipped');
          if(isFlipped) {
            element.attr('aria-expanded', false);
            element.removeClass('is-flipped');
          } else {
            element.attr('aria-expanded', true);
            element.addClass('is-flipped')
          }
        }
  
        $partnerCards.click(function(e){
          $currentCard = $(e.currentTarget);
          toggleFlipped($currentCard);
        });

        $partnerCards.keydown(function(e){
          if(e.type == "keydown") {
            var keyPressed = e.key;
            if(keyPressed == "Enter") {
              $currentCard = $(e.currentTarget);
              toggleFlipped($currentCard);
            }
          }
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);