(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.searchModal = {
    attach: function(context, settings) {
      var $buttonOpenModalSearch =$('.sri-search-button', context);
      var $modals = $('.modals');
      var $modal = $('.modal--views-blocksearch-block', context);

      if($modal.length > 0) {
        var $buttonCloseModal = $modal.find('.button-close', context);
        
        $buttonCloseModal.click(function(e) {
          e.preventDefault();
          var $parentModals = $buttonCloseModal.parents('.modals');
          var $parentModal = $buttonCloseModal.parents('.modal');
          $parentModal.removeClass('is-displayed');
          $parentModals.removeClass('is-active');
        });
      }

      $buttonOpenModalSearch.click(function() {
        $modals.addClass('is-active');
        $modal.addClass('is-displayed');
      });
    },
  }
})(jQuery, Drupal, drupalSettings);
