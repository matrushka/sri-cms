<?php

namespace Drupal\sri_blocks\Form;

use \Drupal\Core\Form\FormBase;
use \Drupal\Core\Form\FormStateInterface;

class ResourcesSearch extends FormBase {
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['keywords'] = [
      '#type' => 'search',
      '#title' => $this->t('Keywords')
      // '#description' => $this->t('Search field description, if needed.')
    ];

    $form['resource_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => [],
      '#empty_option' => $this->t('All'),
    ];

    $vid = 'resource_type';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
     // $term_data[] = array(
     //  'id' => $term->tid,
     //  'name' => $term->name
     // );

      $form['resource_type']['#options'][$term->tid] = $term->name;
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search')
    ];

    return $form;
  }

  public function getFormId() {
    return 'sri_blocks_resources_search_form';
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $options = [
      'query' => [
        'content_type' => 'resource',
        'search_api_fulltext' => $values['keywords']
      ]
    ];
    if(!empty($values['resource_type'])) {
      $options['query']['field_resource_type'] = $values['resource_type'];
    }

    $search_page = \Drupal\Core\Url::fromRoute('view.search.page', [], $options);
    $form_state->setRedirectUrl($search_page);
  }
}
