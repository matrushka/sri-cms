<?php

namespace Drupal\sri_blocks\Form;

use \Drupal;
use \Exception;
use \Drupal\Core\Form\FormBase;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Url;
use \Drupal\Core\Routing\TrustedRedirectResponse;

class NewsletterSubscription extends FormBase {
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['#prefix'] = '<div id="newsletter-subscription-form-wrapper">';
    $form['#suffix'] = '</div>';

    $form['fields'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'fields-container',
      ]
    ];

    $form['actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'actions-container',
      ]
    ];

    $fields = &$form['fields'];
    $actions = &$form['actions'];

    $fields['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#placeholder' => $this->t('Your Email...'),
      '#required' => TRUE
    ];

    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sign up')
    ];

    return $form;
  }

  public function getFormId() {
    return 'sri_blocks_newsletter_subscription_form';
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // $mailchimp_form = Drupal::state()->get('mailchimp.newsletter'); // Setear la variable me falló, pero será mejor que tenerla en hardcode
    $mailchimp_form = "https://sexualrightsinitiative.us3.list-manage.com/subscribe?u=67191046cd42cbb7c7d4196b0&id=31d33dadda";
    $mailchimp_url = $mailchimp_form."&MERGE0=".$values['email'];

    $response = new TrustedRedirectResponse(Url::fromUri($mailchimp_url)->toString());

    $form_state->setResponse($response);

    return $form;
  }
}

