<?php

namespace Drupal\sri_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Partners footer' Block.
 *
 * @Block(
 *   id = "partners_footer_block",
 *   admin_label = @Translation("Partners footer"),
 *   category = @Translation("SRI blocks"),
 * )
 */
class PartnersFooterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $code = 'partners';
    return array(
      '#theme' => 'freeform',
      '#code' => $code,
    );
  }

}
