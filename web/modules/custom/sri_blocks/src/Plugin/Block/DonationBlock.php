<?php

namespace Drupal\sri_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Donation' Block.
 *
 * @Block(
 *   id = "donation_block",
 *   admin_label = @Translation("Donate to SRI"),
 *   category = @Translation("SRI blocks"),
 * )
 */
class DonationBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $code = 'donation';
    return array(
      '#theme' => 'freeform',
      '#code' => $code,
    );;
  }

}
