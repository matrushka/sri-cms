<?php

namespace Drupal\sri_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Search button' Block.
 *
 * @Block(
 *   id = "search_button_block",
 *   admin_label = @Translation("Search button"),
 *   category = @Translation("SRI blocks"),
 * )
 */
class SearchButtonBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $code = 'searchbutton';
    return array(
      '#theme' => 'freeform',
      '#code' => $code,
      '#load_node' => false
    );
  }

}
