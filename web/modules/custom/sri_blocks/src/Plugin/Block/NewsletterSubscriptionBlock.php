<?php

namespace Drupal\sri_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Newsletter Subscription' Block.
 *
 * @Block(
 *   id = "newsletter_subscription_block",
 *   admin_label = @Translation("Subscribe to our newsletter"),
 *   category = @Translation("SRI blocks"),
 * )
 */
class NewsletterSubscriptionBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $code = 'subscribe';
    return array(
      '#theme' => 'freeform',
      '#code' => $code,
    );
  }

}
