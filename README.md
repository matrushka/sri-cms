# SRI new website - Drupal 8 CMS project

Prerequisites:
- Composer
- Mysql empty database
- Apache user and group
- Be a sudoer
- A vmhost configured to access this repo "web" folder
- The latest Mysql dump of the project

### Installing Drupal Core
To install Drupal core files and all of its dependencies run `composer install` in root folder after cloning this repo. *composer.json* has been prepared to install *core* files in the proper directory for this Drupal 8 project to work.

### Create database
Manually create and empty database with mysql for this project. Keep the database name, user and paswoord at hand.

### Change settings and files folder permisions
Go to `sites/default` and make sure a 'files' directory is created. If not, create one (`mkdir files`) then change its permisions to *0777* with the following command. Make sure you are on `sites/default` path:

````
sudo chmod -R 0777 files
````

Now copy `default.settings.php` to settings.php in `sites/default` folder. Then change it's permisions to *0666*:

````
cp default.settings.php settings.php
sudo chmod 0666 settings.php
````

After site instalation is completed (as described in the nex section), return `settings.php` permisions to *0755* to avoid security problems.

### Install Drupal core tables into database and configure dummy site
Access to the vmhost configured domain name trough any web browser, a Drupal 8 install page should be promted. Select default language (english recommended) and click continue. Configure database name, user and password. If required, configure also database server domain and port by clicking the 'advanced settings' option at the bottom. All tables should be generated properly. Then configure any site name and user; this will be overriden in the next step.

### Override database with latest project dump
First, remove all tables from the current database:

````
drush sql-drop
````

Then, execute the sql file with the latest dump:

````
drush sql-cli < ~/latest-sql-dump-file.sql
````

### Installing new Drupal modules
Drupal modules are being managed with composer in this project. In order to register a new drupal module added to the project in the `composer.json` file, it should be installed with composer itself, using the following command:

````
composer require "drupal/module_name"
````

That will download module files under `modules/contrib` and update `composer.json` with the new module name as a requirement. New modules should be enabled then with *drush* or using the web admin UI.
New commits should not include the contrib module downloaded files, only updates to `composer.json` file and module configurations exported to *sync folder* should be included.

### Pulling and updating git submodules included
This project has git submodules, wich means some of the code needed is included in embebed repositories (The SRI theme, is an example). To pull and update any git submodule, just run the following command:

````
git submodule update --init --recursive
````